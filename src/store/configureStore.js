import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import imagesReducer from '../reducers/imagesReducer';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export default () => {
    const store = createStore(
        combineReducers({
            feed: imagesReducer,
        }),
        composeEnhancers(applyMiddleware(thunk))
    );
    return store;
};