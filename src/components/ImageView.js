import React from 'react';
import { Image } from './Image';
import { connect } from 'react-redux';

export function ImageView({ match, imageProp }) {
  let image = imageProp;

  if (!image) return <div>Image not found</div>;

  return (
    <div>
      <h1>{image.title}</h1>
      <Image color={image.color} />
    </div>
  );
}

