import React from 'react';
import { Image } from './Image';
import { connect } from 'react-redux';
import { startRemoveImage } from '../actions/imagesActions';

function Modal({ match, history, imageProp, startReportImage }) {
  let image = imageProp;

  if (!image) return null;

  let back = e => {
    e.stopPropagation();
    history.goBack();
  };

  return (
    <div
      onClick={back}
      style={{
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        background: "rgba(0, 0, 0, 0.15)"
      }}
    >
      <div
        className="modal"
        style={{
          position: "absolute",
          background: "#fff",
          top: 25,
          left: "10%",
          right: "10%",
          padding: 15,
          border: "2px solid #444"
        }}
      >
        <button onClick={() => startReportImage(imageProp.id)}>hiiiiiii</button>
        <h1>{image.title}</h1>
        <Image color={image.color} />
        <button type="button" onClick={back}>
          Close
          </button>
      </div>
    </div>
  );
}

const mapStateToProps = (state, props) => {
  console.log(props);
  return {
    imageProp: state.feed.images.find((image) => image.id === parseInt(props.match.params.id))
  }
};

const mapDispatchToProps = (dispatch) => ({
  startReportImage: (id) => dispatch(startRemoveImage(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Modal);