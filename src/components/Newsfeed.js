import React, { Component } from 'react';
import { getImages } from '../api/api';
import { Link } from 'react-router-dom';
import { Thumbnail } from './Thumbnail';
import { connect } from 'react-redux';
import { startSetImages, startRemoveImage, startToggleFollow } from '../actions/imagesActions'

class Newsfeed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: []
    }
  }

  componentDidMount() {
    this.props.startSetImages()
  }


  render() {
    return (
      <div>
        {this.props.images ? this.props.images.map(i => (
          <React.Fragment key={i.id}>
            <Link

              to={{
                pathname: `/img/${i.id}`,
                // this is the trick!
                state: { modal: true }
              }}
            >
              <Thumbnail color={i.color} />
              <p>{i.title}</p>
            </Link>
            <p>{!i.follow ? "hi" : ""}</p>
            <button onClick={() => this.props.startReportImage(i.id)}>Delete</button>
            <button onClick={() => this.props.toggle()}>Toggle</button>

          </React.Fragment >
        )) : <div>Nothing</div>}
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    images: state.feed.images,
  };
};

const mapDispatchToProps = (dispatch) => ({
  startSetImages: () => dispatch(startSetImages()),
  startReportImage: (id) => dispatch(startRemoveImage(id)),
  toggle: () => dispatch(startToggleFollow())
})


export default connect(mapStateToProps, mapDispatchToProps)(Newsfeed);