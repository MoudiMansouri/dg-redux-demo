import React, { Component } from "react";
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import AppRouter from "./router/router";
import configureStore from "./store/configureStore";
import { norm } from "./norm";

const store = configureStore();
const state = store.getState();

function ModalGallery() {
  norm();
  return (
    <Provider store={store}>
      <Router>
        {/* <Route component={AppRouter} /> */}
      </Router>
    </Provider>
  );
}

export default ModalGallery;