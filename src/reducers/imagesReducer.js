// Expenses Reducer
const imageReducerDefaultState = {
    images: [],
    more: true
};

export default (state = imageReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_IMAGE':
            return [
                ...state,
                action.images
            ]
        case 'REMOVE_IMAGE':
            return state.filter(({ id }) => id !== action.id);
        case 'TOGGLE':
            return Object.assign({}, state, {
                images: state.images.map((image) => (image.id === 0) ? { ...image, follow: !image.info.follow } : image)
            })
        case 'SET_IMAGES':
            return Object.assign({}, state, { images: action.images });
        default:
            return state;
    }
}
