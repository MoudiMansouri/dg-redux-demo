import { getImages, reportImage, toggle } from '../api/api';

export const removeImage = (id) => ({
    type: 'REMOVE_IMAGE',
    id
});

export const startRemoveImage = (id) => {
    console.log(id);
    return (dispatch) => {
        return reportImage(id).then((images) => {
            console.log('Dispatching', id);
            dispatch(removeImage(id));
        })
    }
};

export const setImages = (images) => ({
    type: 'SET_IMAGES',
    images
});

export const startSetImages = () => {
    return (dispatch) => {
        return getImages().then((images) => {
            const returnedImages = [];

            images.forEach((image) => {
                returnedImages.push(image)
            });
            dispatch(setImages(returnedImages));
        });
    }
};


export const toggleFollow = () => ({
    type: 'TOGGLE'
})

export const startToggleFollow = () => {
    return (dispatch) => {
        return toggle().then(() => {
            dispatch(toggleFollow())
        })
    }
}