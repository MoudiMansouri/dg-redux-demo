const IMAGES = [
        { id: 0, title: "Dark Orchid", color: "DarkOrchid", info: { follow: false } },
        { id: 1, title: "Lime Green", color: "LimeGreen", info: { follow: false } },
        { id: 2, title: "Tomato", color: "Tomato", info: { follow: false } },
        { id: 3, title: "Seven Ate Nine", color: "#789", info: { follow: false } },
        { id: 4, title: "Crimson", color: "Crimson", info: { follow: false } }
];

async function stall(stallTime = 3000) {
        await new Promise(resolve => setTimeout(resolve, stallTime));
}

export const getImages = async (next) => {
        if (next) {
                return new Promise(resolve => {
                        setTimeout(() => {
                                resolve([IMAGES[3], IMAGES[4], IMAGES[5]]);
                        }, 300);
                })
        } else {
                return new Promise(resolve => {
                        setTimeout(() => {
                                resolve([IMAGES[0], IMAGES[1], IMAGES[2]]);
                        }, 300);
                })
        }
}

export const reportImage = async (id) => {
        return new Promise(resolve => {
                setTimeout(() => {
                        resolve(IMAGES.filter((img) => {
                                return img.id !== id;
                        }));
                }, 300);
        })
}

export const toggle = async () => {
        return new Promise(resolve => {
                resolve(IMAGES[0].info.follow = !IMAGES[0].info.follow)
        })
}