import { normalize, schema } from 'normalizr';

const data = {

    "_events": {},

    "id": "UPL_181adb00-5ae3-11e9-b1c4-f9d393a6f5cc",

    "bestAnswerIdPublic": null,

    "userProfile": {

        "_events": {},

        "avatar": "/static/media/default_anonymous.d98ec1f2.png",

        "firstName": null,

        "lastName": null,

        "prenominalTitle": null,

        "postnominalTitle": null,

        "status": null,

        "profession": {

            "id": 7,

            "status": "verified",

            "value": "Assistenzarzt"

        },

        "specialty": {

            "id": 3,

            "value": "Allergology_label",

            "translated": "Allergologie & Immunologie"

        },

        "specialties": [

            {

                "id": 3,

                "value": "Allergology_label",

                "translated": "Allergologie & Immunologie"

            }

        ],

        "degree": null,

        "isVerified": false,

        "inProcess": false,

        "notVerified": false

    },

    "bestAnswerReported": false,

    "description": "ðŸ±  ðŸˆ  ðŸ˜º ",

    "isFollowed": false,

    "isSaved": false,

    "mentions": [],

    "numComments": 0,

    "numFollows": 2,

    "numLikes": 0,

    "numVisits": 1,

    "images": [

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=1801d4c0-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=1803d090-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=18099cf0-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=180c5c10-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=18124f80-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU"

    ],

    "isPublicAuthor": false,

    "scope": {

        "delete": false,

        "report": true,

        "acceptAnswer": false,

        "anonimous": true

    },

    "status": "verified",

    "categories": [

        {

            "id": 1,

            "value": "anesthesia",

            "translated": "Anesthesiologie"

        },

        {

            "id": 48,

            "value": "anatomy",

            "translated": "Anatomie"

        }

    ],

    "thumbnail": "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=thumb_small_18015f90-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

    "thumbnailAlbum": [

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=thumb_med_18035b60-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=thumb_med_18094ed0-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=thumb_med_180bbfd0-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=thumb_med_18111700-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU",

        "http://develop.doctorsgate.com/getFile?R=1&B=4&id=USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219&f=case-17ffd8f0-5ae3-11e9-b1c4-f9d393a6f5cc&n=thumb_med_181a3ec0-5ae3-11e9-b1c4-f9d393a6f5cc.jpg&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWNhZjU2MzY4MjAzY2YzZWUzY2FhMDRiIiwic2lnbiI6ImY3MDMwOGJhMmFlZDE2MjU4M2FhNzg5OGQ2NDI3MDA0ODlhMTRmMzVmNTFiZDUxZjg5NTU4YWIyNjRiMWJhNjMiLCJpYXQiOjE1NTQ5OTc5MjMsImV4cCI6MTU1NDk5ODIyM30.-AGNptmZp7hA1pZsIcp-PAc2FopsQ5SA5IyAA09L7IU"

    ],

    "title": "cats",

    "typeCase": "case"

}



const data2 = {

    "_events": {},

    "id": "UPL_50ce5900-5ae2-11e9-b1c4-f9d393a6f5cc",

    "bestAnswerIdPublic": null,

    "userProfile": {

        "_events": {},

        "id": "USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219",

        "avatar": "/static/media/avatar_example.fc047347.png",

        "firstName": "Captain",

        "lastName": "Marvel",

        "prenominalTitle": null,

        "postnominalTitle": null,

        "status": null,

        "title": "",

        "id": 1524,

        "display": "captainmarvelscedst3",

        "profession": {

            "id": 7,

            "status": "verified",

            "value": "Assistenzarzt"

        },

        "specialty": {

            "id": 3,

            "value": "Allergology_label",

            "translated": "Allergologie & Immunologie"

        },

        "specialties": [

            {

                "id": 3,

                "value": "Allergology_label",

                "translated": "Allergologie & Immunologie"

            }

        ],

        "degree": null,

        "isVerified": false,

        "inProcess": false,

        "notVerified": false

    },

    "bestAnswerReported": false,

    "description": "meow",

    "isFollowed": false,

    "isSaved": false,

    "mentions": [],

    "numComments": 0,

    "numFollows": 1,

    "numLikes": 0,

    "numVisits": 0,

    "images": [],

    "isPublicAuthor": true,

    "scope": {

        "delete": false,

        "report": true,

        "acceptAnswer": false,

        "anonimous": false

    },

    "status": "verified",

    "categories": [

        {

            "id": 48,

            "value": "anatomy",

            "translated": "Anatomie"

        }

    ],

    "thumbnailAlbum": [],

    "title": "cat",

    "typeCase": "case"

}

const data3 = {

    "_events": {},

    "id": "diffID",

    "bestAnswerIdPublic": null,

    "userProfile": {

        "_events": {},

        "id": "USR_0dd9f420-4c90-11e9-9dcc-81b108a0e219",

        "avatar": "/static/media/avatar_example.fc047347.png",

        "firstName": "Captain",

        "lastName": "Marvel",

        "prenominalTitle": null,

        "postnominalTitle": null,

        "status": null,

        "title": "",

        "id": 1524,

        "display": "captainmarvelscedst3",

        "profession": {

            "id": 7,

            "status": "verified",

            "value": "Assistenzarzt"

        },

        "specialty": {

            "id": 3,

            "value": "Allergology_label",

            "translated": "Allergologie & Immunologie"

        },

        "specialties": [

            {

                "id": 3,

                "value": "Allergology_label",

                "translated": "Allergologie & Immunologie"

            }

        ],

        "degree": null,

        "isVerified": false,

        "inProcess": false,

        "notVerified": false

    },

    "bestAnswerReported": false,

    "description": "meow",

    "isFollowed": true,

    "isSaved": false,

    "mentions": [],

    "numComments": 0,

    "numFollows": 1,

    "numLikes": 0,

    "numVisits": 0,

    "images": [],

    "isPublicAuthor": true,

    "scope": {

        "delete": false,

        "report": true,

        "acceptAnswer": false,

        "anonimous": false

    },

    "status": "verified",

    "categories": [

        {

            "id": 48,

            "value": "anatomy",

            "translated": "Anatomie"

        }

    ],

    "thumbnailAlbum": [],

    "title": "cat",

    "typeCase": "case"

}







export const norm = () => {
    const cases = [data, data2, data3];
    const users = [data, data2, data3];

    const user = new schema.Entity('users', undefined,
        {
            idAttribute: value => {
                return value.id ? value.id : 'Dick'
            }
        });


    const case_ = new schema.Entity('case', {
        userProfile: user,
    }, {
            idAttribute: value => {
                return value.id
            }
        });

    const normalizee = normalize(users, [user]);
    console.log(normalizee);

    const normalizee2 = normalize(cases, [case_]);

    console.log(normalizee2);
}